package com.oleksii.vendor.controllers;

import com.oleksii.vendor.entities.Vendor;
import com.oleksii.vendor.service.VendorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
public class VendorController {

    @Autowired
    VendorService service;

    @GetMapping("/showCreate")
    public String showCreate() {
        return "createVendor";
    }

    @GetMapping("/showUpdate")
    public String showUpdate(@RequestParam("id") int id, ModelMap modelMap) {
        Vendor vendor = service.getVendorById(id);
        modelMap.addAttribute("vendor", vendor);
        return "updateVendor";
    }

    @PostMapping("/updateVendor")
    public String updateVendor(@ModelAttribute("vendor") Vendor vendor, ModelMap modelMap) {
        System.out.println(vendor);
        service.updateVendor(vendor);
        List<Vendor> vendors = service.getAllVendors();
        modelMap.addAttribute("vendors", vendors);
        return "displayVendor";
    }

    @PostMapping("/saveVendor")
    public String saveVendor(@ModelAttribute("vendor") Vendor vendor, ModelMap modelMap) {
        Vendor savedVendor = service.saveVendor(vendor);
        String msg = String.format("Vendor is saved with id: %s", savedVendor.getId());
        modelMap.addAttribute("msg", msg);
        return "createVendor";
    }

    @GetMapping("/displayVendors")
    public String displayVendors(ModelMap modelMap) {
        List<Vendor> vendors = service.getAllVendors();
        modelMap.addAttribute("vendors", vendors);
        return "displayVendor";
    }

    @GetMapping("/deleteVendor")
    public String deleteVendor(@RequestParam("id") int id, ModelMap modelMap) {
        service.deleteVendor(service.getVendorById(id));
        List<Vendor> vendors = service.getAllVendors();
        modelMap.addAttribute("vendors", vendors);
        return "displayVendor";
    }

}
