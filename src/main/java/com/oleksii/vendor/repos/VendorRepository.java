package com.oleksii.vendor.repos;

import com.oleksii.vendor.entities.Vendor;
import org.springframework.data.jpa.repository.JpaRepository;

public interface VendorRepository extends JpaRepository<Vendor, Integer> {
}
